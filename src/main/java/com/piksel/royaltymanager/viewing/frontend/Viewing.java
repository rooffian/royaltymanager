package com.piksel.royaltymanager.viewing.frontend;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
class Viewing {

    private final String episode;
    private final String customer;

    @JsonCreator
    public Viewing(
            @JsonProperty(value = "episode", required = true) final String episode,
            @JsonProperty(value = "customer", required = true) final String customer) {
        this.episode = episode;
        this.customer = customer;
    }
}
