package com.piksel.royaltymanager.viewing.frontend;

import java.util.UUID;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.piksel.royaltymanager.common.InalidGuidException;
import com.piksel.royaltymanager.common.UuidTool;
import com.piksel.royaltymanager.content.repository.Episode;
import com.piksel.royaltymanager.content.repository.EpisodeRepository;
import com.piksel.royaltymanager.viewing.repository.ViewCountRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "Viewing Resource", description = "Allows providing views information")
class ViewingController {

    @Autowired
    private ViewCountRepository viewCountRepository;
    @Autowired
    private EpisodeRepository episodeRepository;

    @ApiOperation(value = "", notes = "Provides information about one tracked view of the given episode", tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 202, message = "Accepted"),
            @ApiResponse(code = 400, message = "Bad request"),
    })
    @RequestMapping(value = "/viewing",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.POST)
    public ResponseEntity viewed(
            @ApiParam(name = "viewing", value = "Object containing identification of a viewed episode and its owner", required = true)
            @RequestBody final Viewing viewing) {
        try {
            final UUID episodeId = UuidTool.fromGuidString(viewing.getEpisode());
            final Episode episode = episodeRepository.findOneAsOptional(episodeId)
                    .orElseThrow(() -> new EntityNotFoundException(String.format("Episode with guid %s not found.", viewing.getEpisode())));

            viewCountRepository.incViewForStudio(episode.getRightsowner());
            return ResponseEntity.accepted().build();
        } catch (InalidGuidException | EntityNotFoundException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @ApiOperation(value = "", notes = "Resets all royalties counters", tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 202, message = "Accepted"),
    })
    @RequestMapping(value = "/reset",
            method = RequestMethod.POST)
    public ResponseEntity reset() {
        viewCountRepository.deleteAll();
        return ResponseEntity.accepted().build();
    }
}
