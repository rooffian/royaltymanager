package com.piksel.royaltymanager.viewing.repository;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "VIEW_COUNT")
public class ViewCount {
    @Id
    @Column(name = "STUDIO_ID")
    private UUID studioId;
    @Column(name = "COUNT")
    private long count;
}
