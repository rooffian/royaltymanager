package com.piksel.royaltymanager.viewing.repository;

import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

@Transactional
public interface ViewCountRepository extends CrudRepository<ViewCount, UUID> {

    @Modifying(clearAutomatically = true)
    @Query(value = "INSERT INTO VIEW_COUNT (STUDIO_ID, COUNT) VALUES (:studioId, 1) ON DUPLICATE KEY UPDATE COUNT = COUNT + 1",
            nativeQuery =  true)
    void incViewForStudio(@Param("studioId") final UUID studioId);

}
