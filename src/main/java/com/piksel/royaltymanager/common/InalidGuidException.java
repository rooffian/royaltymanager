package com.piksel.royaltymanager.common;

public class InalidGuidException extends Exception {

    public InalidGuidException(final String message) {
        super(message);
    }
}
