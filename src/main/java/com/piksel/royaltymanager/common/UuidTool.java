package com.piksel.royaltymanager.common;

import java.io.InvalidObjectException;
import java.util.UUID;

public class UuidTool {

    public static UUID fromGuidString(final String guid) throws InalidGuidException {
        if (guid == null || guid.length() != 32) {
            throw new InalidGuidException("Provided value is not a proper guid: " + guid);
        }
        final String withHyphens = guid
                .replaceFirst("([0-9a-fA-F]{8})([0-9a-fA-F]{4})([0-9a-fA-F]{4})([0-9a-fA-F]{4})([0-9a-fA-F]+)", "$1-$2-$3-$4-$5");
        return UUID.fromString(withHyphens);
    }

    public static String toGuidString(final UUID uuid) {
        return uuid.toString().replaceAll("-","");
    }
}
