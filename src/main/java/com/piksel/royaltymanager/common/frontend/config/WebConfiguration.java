package com.piksel.royaltymanager.common.frontend.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import static com.piksel.royaltymanager.common.frontend.config.SwaggerConfiguration.GROUP_NAME;

@Configuration
class WebConfiguration extends WebMvcConfigurationSupport {

    private final String swaggerApiPath;
    private final String documentationContextPath;

    public WebConfiguration(
            @Value("${web.documentation.context-path}") final String documentationContextPath,
            @Value("${springfox.documentation.swagger.v2.path}") final String swaggerApiPath) {
        this.documentationContextPath = documentationContextPath;
        this.swaggerApiPath = swaggerApiPath;
    }
    //
    //    @Override
    //    protected RequestMappingHandlerMapping createRequestMappingHandlerMapping() {
    //        return new ApiVersionHandlerMapping(versionPrefix);
    //    }

    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        registry.setOrder(1);
        register(registry, "/api-docs", swaggerApiPath + "?group=" + GROUP_NAME);
        register(registry, "/swagger-resources/configuration/ui", "/swagger-resources/configuration/ui");
        register(registry, "/swagger-resources/configuration/security", "/swagger-resources/configuration/security");
        register(registry, "/swagger-resources", "/swagger-resources");
        register(registry, "", appendDocumentationContextPath("/swagger-ui.html"));
        register(registry, "/", appendDocumentationContextPath("/swagger-ui.html"));
    }

    private void register(final ViewControllerRegistry registry, final String url, final String redirectUrl) {
        registry.addRedirectViewController(appendDocumentationContextPath(url), redirectUrl);
    }

    private String appendDocumentationContextPath(final String url) {
        return documentationContextPath + url;
    }

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler(appendDocumentationContextPath("/swagger-ui.html**"))
                .addResourceLocations("classpath:/META-INF/resources/swagger-ui.html");
        registry.addResourceHandler(appendDocumentationContextPath("/webjars/**"))
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/api-docs/**");
    }
}
