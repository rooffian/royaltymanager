package com.piksel.royaltymanager.common.frontend.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.base.Predicates.and;
import static com.google.common.base.Predicates.not;
import static springfox.documentation.builders.PathSelectors.regex;

@EnableSwagger2
@Configuration
class SwaggerConfiguration {

    public static final String GROUP_NAME = "royaltymanager";

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(GROUP_NAME)
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .select()
                .paths(and(regex("/.*"), not(regex("/error"))))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Piksel - Royalty Manager")
                .description("Application for counting viewing royalties for the rights owners")
                .version(getClass().getPackage().getImplementationVersion())
                .build();
    }
}
