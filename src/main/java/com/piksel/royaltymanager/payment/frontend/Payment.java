package com.piksel.royaltymanager.payment.frontend;

import java.math.BigDecimal;

import com.piksel.royaltymanager.payment.repository.StudioPayment;

import lombok.Data;

@Data
class Payment {

    private final String rightsowner;
    private final BigDecimal royalty;
    private final long viewings;

    public static Payment fromStudioPayment(final StudioPayment studioPayment) {
        return new Payment(studioPayment.getStudioName(),
                studioPayment.getRoyalty(),
                studioPayment.getViewings());
    }
}
