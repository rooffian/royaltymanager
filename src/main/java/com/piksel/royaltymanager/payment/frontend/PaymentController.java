package com.piksel.royaltymanager.payment.frontend;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.piksel.royaltymanager.common.InalidGuidException;
import com.piksel.royaltymanager.common.UuidTool;
import com.piksel.royaltymanager.payment.repository.PaymentRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "Payment Resource", description = "Allows retrieving information about Rights Owners' royalties")
class PaymentController {

    @Autowired
    private PaymentRepository paymentRepository;

    @ApiOperation(value = "", notes = "Returns information about all Rights Owners' royalties", response = PaymentWithId.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = PaymentWithId.class),
    })
    @RequestMapping(value = "/payments",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET)
    public List<PaymentWithId> getAllPayments() {
        return paymentRepository.findAll().stream()
                .map(PaymentWithId::fromStudioPayment)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "", notes = "Returns information about selected Rights Owner's royalties", response = Payment.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Payment.class),
            @ApiResponse(code = 404, message = "Not found"),
    })
    @RequestMapping(value = "/payments/{rightsOwnerGuid}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET)
    public ResponseEntity<Payment> getPaymentsByOwner(
            @ApiParam(value = "The GUID of the Rights Owner", required = true)
            @PathVariable("rightsOwnerGuid") final String rightsOwnerGuid) {
        try {
            return paymentRepository.findOne(UuidTool.fromGuidString(rightsOwnerGuid))
                    .map(Payment::fromStudioPayment)
                    .map(p -> ResponseEntity.ok(p))
                    .orElse(ResponseEntity.notFound().build());
        } catch (InalidGuidException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
