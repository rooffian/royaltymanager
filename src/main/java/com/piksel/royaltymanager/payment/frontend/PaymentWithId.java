package com.piksel.royaltymanager.payment.frontend;

import java.math.BigDecimal;

import com.piksel.royaltymanager.common.UuidTool;
import com.piksel.royaltymanager.payment.repository.StudioPayment;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
class PaymentWithId extends Payment {

    private final String rightsownerId;

    public PaymentWithId(final String rightsownerId, final String rightsowner, final BigDecimal royalty, final long viewings) {
        super(rightsowner, royalty, viewings);
        this.rightsownerId = rightsownerId;
    }

    public static PaymentWithId fromStudioPayment(final StudioPayment studioPayment) {
        return new PaymentWithId(UuidTool.toGuidString(studioPayment.getStudioId()),
                studioPayment.getStudioName(),
                studioPayment.getRoyalty(),
                studioPayment.getViewings());
    }
}
