package com.piksel.royaltymanager.payment.repository;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudioPayment {

    @Id
    @Column(name = "ID")
    private UUID studioId;
    @Column(name = "NAME")
    private String studioName;
    @Column(name = "ROYALTY")
    private BigDecimal royalty;
    @Column(name = "VIEWINGS")
    private long viewings;
}
