package com.piksel.royaltymanager.payment.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

public interface PaymentRepository extends Repository<StudioPayment, UUID> {

    String PAYMENTS_SQL = "SELECT S.ID, S.NAME,COALESCE(VC.COUNT, 0) AS VIEWINGS," +
            " COALESCE(VC.COUNT, 0)*S.PAYMENT AS ROYALTY" +
            " FROM VIEW_COUNT VC RIGHT JOIN STUDIO S ON VC.STUDIO_ID = S.ID";

    @Query(value = PAYMENTS_SQL + " WHERE S.ID = :studioId", nativeQuery = true)
    Optional<StudioPayment> findOne(@Param("studioId") final UUID studioId);

    @Query(value = PAYMENTS_SQL, nativeQuery = true)
    List<StudioPayment> findAll();
}
