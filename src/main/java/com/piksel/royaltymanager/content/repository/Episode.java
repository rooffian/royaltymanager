package com.piksel.royaltymanager.content.repository;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.piksel.royaltymanager.common.InalidGuidException;
import com.piksel.royaltymanager.common.UuidTool;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "EPISODE")
public class Episode {

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    private UUID id;
    @Column(name = "NAME", unique = false, nullable = true)
    private String name;
    @Column(name = "RIGHTSOWNER", unique = false, nullable = false)
    private UUID rightsowner;

    @JsonCreator
    public Episode(@JsonProperty("id") final String id,
            @JsonProperty("name") final String name,
            @JsonProperty("rightsowner") final String rightsowner) throws InalidGuidException {
        this.id = UuidTool.fromGuidString(id);
        this.rightsowner = UuidTool.fromGuidString(rightsowner);
        this.name = name;
    }
}
