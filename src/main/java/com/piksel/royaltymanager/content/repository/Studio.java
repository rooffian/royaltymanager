package com.piksel.royaltymanager.content.repository;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.piksel.royaltymanager.common.InalidGuidException;
import com.piksel.royaltymanager.common.UuidTool;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "STUDIO")
public class Studio {

    @Id
    private UUID id;
    private String name;
    private BigDecimal payment;

    @JsonCreator
    public Studio(@JsonProperty("id") final String id,
            @JsonProperty("name") final String name,
            @JsonProperty("payment") final String payment) throws InalidGuidException {
        this.id = UuidTool.fromGuidString(id);
        this.payment = new BigDecimal(payment);
        this.name = name;
    }
}
