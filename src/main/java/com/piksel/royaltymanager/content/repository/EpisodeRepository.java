package com.piksel.royaltymanager.content.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface EpisodeRepository extends CrudRepository<Episode, UUID> {

    @Query("SELECT episode FROM Episode episode WHERE episode.id = :episodeId")
    Optional<Episode> findOneAsOptional(@Param("episodeId") final UUID episodeId);
}
