package com.piksel.royaltymanager.content.repository;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

public interface StudioRepository extends CrudRepository<Studio, UUID> {
}
