package com.piksel.royaltymanager.viewing.frontend;

import java.util.Optional;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import com.piksel.royaltymanager.content.repository.Episode;
import com.piksel.royaltymanager.content.repository.EpisodeRepository;
import com.piksel.royaltymanager.viewing.repository.ViewCountRepository;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ViewingController.class)
public class ViewingControllerTest {

    private static final String SAMPLE_GUID = "75aee18236484501b209aa36f95c7e0f";
    @MockBean
    private ViewCountRepository viewCountRepository;
    @MockBean
    private EpisodeRepository episodeRepository;
    @Autowired
    private MockMvc mvc;

    @Test
    public void resetShouldRemoveAllViewCounts() throws Exception {
        //when
        final ResultActions result = mvc.perform(post("/reset"));

        //then
        result.andExpect(status().isAccepted());
        verify(viewCountRepository, atLeastOnce()).deleteAll();
    }

    @Test
    public void viewingWithWrongDataReturnsBadRequest() throws Exception {
        //given
        when(episodeRepository.findOneAsOptional(any(UUID.class))).thenReturn(Optional.empty());

        checkViewingWithWrongDataReturnsBadRequest("");
        checkViewingWithWrongDataReturnsBadRequest("{}");
        checkViewingWithWrongDataReturnsBadRequest("{\"episode\": \"guid\"}");
        checkViewingWithWrongDataReturnsBadRequest("{\"customer\": \"guid\"}");
        checkViewingWithWrongDataReturnsBadRequest("{\"episode\": \"guid\", \"customer\": \"guid\"}");
        checkViewingWithWrongDataReturnsBadRequest("{\"episode\": \"guid\", \"customer\": \"" + SAMPLE_GUID + "\"}");
        checkViewingWithWrongDataReturnsBadRequest("{\"episode\": \"" + SAMPLE_GUID + "\", \"customer\": \"" + SAMPLE_GUID + "\"}");
    }

    private void checkViewingWithWrongDataReturnsBadRequest(final String requestBody) throws Exception {
        //when
        final ResultActions result = postViewing(requestBody);

        //then
        result.andExpect(status().isBadRequest());
        verify(viewCountRepository, never()).incViewForStudio(any(UUID.class));
    }

    @Test
    public void viewingIncreasesViewCounterForProperOwner() throws Exception {
        //given
        final Episode episode = new Episode(SAMPLE_GUID, "Name", SAMPLE_GUID);
        when(episodeRepository.findOneAsOptional(any(UUID.class))).thenReturn(Optional.of(episode));

        final ResultActions result = postViewing(buildViewingRequest(SAMPLE_GUID, SAMPLE_GUID));

        //then
        result.andExpect(status().isAccepted())
                .andExpect(content().string(""));
        verify(viewCountRepository, atLeastOnce()).incViewForStudio(any(UUID.class));
    }

    private ResultActions postViewing(final String requestBody) throws Exception {
        return mvc.perform(post("/viewing")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody));
    }

    private String buildViewingRequest(final String episodeId, final String studioId) {
        return String.format("{\"episode\": \"%s\", \"customer\": \"%s\"}", episodeId, studioId);
    }
}
