package com.piksel.royaltymanager.payment.frontend;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import com.google.common.collect.ImmutableList;
import com.piksel.royaltymanager.common.UuidTool;
import com.piksel.royaltymanager.payment.repository.PaymentRepository;
import com.piksel.royaltymanager.payment.repository.StudioPayment;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PaymentController.class)
public class PaymentControllerTest {

    private static final UUID SAMPLE_GUID1 = UUID.fromString("75aee182-3648-4501-b209-aa36f95c7e0f");
    private static final UUID SAMPLE_GUID2 = UUID.fromString("8d713a09-2ebf-4844-840c-b90d0c4a2030");
    private static final UUID SAMPLE_GUID3 = UUID.fromString("66511572-1c6f-44e4-9be3-bd3e26606026");
    private static final String EXPECTED_PAYMENTS_JSON = "[{\"rightsowner\":\"Studio1\",\"royalty\":10.22,\"viewings\":2," +
            "\"rightsownerId\":\"75aee18236484501b209aa36f95c7e0f\"},{\"rightsowner\":\"Studio2\",\"royalty\":60.00,\"viewings\":6," +
            "\"rightsownerId\":\"8d713a092ebf4844840cb90d0c4a2030\"},{\"rightsowner\":\"Studio3\",\"royalty\":3.33,\"viewings\":3," +
            "\"rightsownerId\":\"665115721c6f44e49be3bd3e26606026\"}]";
    private static final String EXPECTED_PAYMENT1_JSON = "{\"rightsowner\":\"Studio1\",\"royalty\":10.22,\"viewings\":2}";
    @MockBean
    private PaymentRepository paymentRepository;
    @Autowired
    private MockMvc mvc;

    @Test
    public void paymentsReturnsListOfPayments() throws Exception {
        //given
        final StudioPayment payment1 = new StudioPayment(SAMPLE_GUID1, "Studio1", new BigDecimal("10.22"), 2L);
        final StudioPayment payment2 = new StudioPayment(SAMPLE_GUID2, "Studio2", new BigDecimal("60.00"), 6L);
        final StudioPayment payment3 = new StudioPayment(SAMPLE_GUID3, "Studio3", new BigDecimal("3.33"), 3L);
        when(paymentRepository.findAll()).thenReturn(ImmutableList.of(payment1, payment2, payment3));

        //when
        final ResultActions result = mvc.perform(get("/payments")
                .accept(MediaType.APPLICATION_JSON));

        //then
        result.andExpect(status().isOk())
                .andExpect(content().string(EXPECTED_PAYMENTS_JSON));
    }

    @Test
    public void paymentsWithParamReturnsPayments() throws Exception {
        //given
        final StudioPayment payment1 = new StudioPayment(SAMPLE_GUID1, "Studio1", new BigDecimal("10.22"), 2L);
        when(paymentRepository.findOne(any(UUID.class))).thenReturn(Optional.of(payment1));

        //when
        final ResultActions result = mvc.perform(get("/payments/" + UuidTool.toGuidString(SAMPLE_GUID1))
                .accept(MediaType.APPLICATION_JSON));

        //then
        result.andExpect(status().isOk())
                .andExpect(content().string(EXPECTED_PAYMENT1_JSON));
    }

    @Test
    public void paymentsWithWrongParamReturnsNotFound() throws Exception {
        //given
        when(paymentRepository.findOne(any(UUID.class))).thenReturn(Optional.empty());

        //when
        final ResultActions result = mvc.perform(get("/payments/" + UuidTool.toGuidString(SAMPLE_GUID1))
                .accept(MediaType.APPLICATION_JSON));

        //then
        result.andExpect(status().isNotFound())
                .andExpect(content().string(""));
    }
}
