package com.piksel.royaltymanager.common;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class UuidToolTest {

    @Test
    public void fromGuidStringConvertsValueCorrectly() throws InalidGuidException{
        //given
        final String guid = "49924ec6ec6c4efca4aa8b0779c89406";

        //when
        final UUID converted = UuidTool.fromGuidString(guid);

        //then
        assertThat(UuidTool.toGuidString(converted)).isEqualTo(guid);
    }
}
