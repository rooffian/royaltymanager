# Project RoyaltyManager


## Getting started


### Running instruction
For running application on port 8090:

```
 mvn spring-boot:run
```
or
```
 ./gradlew bootRun
```

### Info
Contains embedded h2 database with sample data.

### Access to API - Swagger UI

```
http://localhost:8090/royaltymanager/documentation/swagger-ui.html#/
```
